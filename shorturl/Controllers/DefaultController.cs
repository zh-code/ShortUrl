﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using shorturl.Data;
using shorturl.Model;

namespace shorturl.Controllers
{
    [ApiController]
    public class DefaultController : ControllerBase
    {
        private UrlContext Context = new UrlContext();
        
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpGet("~/Get")]
        public async Task<IActionResult> Get(string Url)
        {
            Url = Url.ToLower();
            if (!Url.Contains("https://") && !Url.Contains("http://"))
            {
                Url = $"http://{Url}";
            }
            RecordEntity r = await Context.Records.FirstOrDefaultAsync(o => o.Url == Url.ToLower());
            if (r != null)
            {
                if (!r.Usable)
                {
                    r.Usable = true;
                    await Context.SaveChangesAsync();
                }
                return Ok("https://s.zh-code.com/" + r.Id);
            }
            try
            {
                r = new RecordEntity(Url.ToLower());
                Context.Records.Add(r);
                await Context.SaveChangesAsync();
                return Ok("https://s.zh-code.com/" + r.Id);
            }
            catch { return StatusCode(500); }
        }

        [ProducesResponseType(404)]
        [HttpGet("~/{Id}")]
        public async Task<IActionResult> Redirect(int Id)
        {
            RecordEntity r = await Context.Records.FindAsync(Id);

            if (r != null && r.Usable)
            {
                r.Count += 1;
                await Context.SaveChangesAsync();
                return Redirect(r.Url);
            }

            return NotFound("Url Not Found");
        }

        [ProducesResponseType(404)]
        [HttpGet("~/Delete/{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            RecordEntity r = await Context.Records.FindAsync(Id);

            if (r != null)
            {
                r.Usable = false;
                await Context.SaveChangesAsync();
            }

            return NotFound("Url Not Found");
        }
    }
}