﻿using Microsoft.EntityFrameworkCore;
using shorturl.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shorturl.Data
{
    public class UrlContext : DbContext
    {
        public DbSet<RecordEntity> Records { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=./DB/url.db");
        }
    }
}
