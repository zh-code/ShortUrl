﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace shorturl.Model
{
    public class RecordEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        public int Count { get; set; }
        
        [Required]
        public DateTime DateInserted { get; set; }

        [Required]
        public bool Usable { get; set; }

        public RecordEntity() { }

        public RecordEntity(string url)
        {
            Url = url;
            Count = 0;
            DateInserted = DateTime.UtcNow;
            Usable = true;
        }
    }
}
